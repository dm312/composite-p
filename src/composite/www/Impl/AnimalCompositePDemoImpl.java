package composite.www.Impl;

import composite.www.CompositePDemo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Danny
 */
public class AnimalCompositePDemoImpl implements CompositePDemo{
    public static void main(String[] args)
    {
        AnimalExampleImpl Panther = new AnimalExampleImpl(1,"BlackPanther","Rawr!!!");
        
        AnimalExampleImpl LittleFox = new AnimalExampleImpl(2,"LittleFox","Pewpew!!!");
       
        AnimalExampleImpl Monkey = new AnimalExampleImpl(3, "Monkey","OO-OO-AA-AA!!!");
        
      
        AnimalExampleImpl Chimp1 = new AnimalExampleImpl(4,"Chimp1","euueu");
        AnimalExampleImpl Chimp2 = new AnimalExampleImpl(5,"Chimp2","euueu");
        
        AnimalExampleImpl SoftShellTurtle1 = new AnimalExampleImpl(6,"BabySoftShellTurtle","!!!");
        AnimalExampleImpl SoftShellTurtle2 = new AnimalExampleImpl(7,"AdultSoftShellTurtle","!!!");
        
        Panther.add(LittleFox);
        Panther.add(Monkey);
        
        Monkey.add(Chimp1);
        Monkey.add(Chimp2);
        
        LittleFox.add(SoftShellTurtle1);
        LittleFox.add(SoftShellTurtle2);
        
        System.out.println(Panther);
        for(AnimalExampleImpl tertiary : Panther.getPrey())
        {
            System.out.println(tertiary.toString());
            for(AnimalExampleImpl secondary : tertiary.getPrey())
            {
                System.out.println(secondary.toString());
            }
        }
    }
}
