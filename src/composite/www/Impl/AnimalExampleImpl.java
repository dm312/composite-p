/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite.www.Impl;
import composite.www.Example;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Danny
 */
public class AnimalExampleImpl implements Example {

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer setId(Integer Id) {
        this.id = Id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
    public List<AnimalExampleImpl> getPrey()
    {
        return this.prey;
    }
    public void setPrey(List<AnimalExampleImpl> prey)
    {
        this.prey = prey;
    }
    public String toString()
    {
        return ("Animal :[ Name : " + name + ", id : " + id + ", call : " + call + " ]");
    }
    public String getCall()
    {
        return this.call;
    }
    public void setCall(String call)
    {
        this.call = call;
    }
    public void add(AnimalExampleImpl animalExampleImpl)
    {
        this.prey.add(animalExampleImpl);
    }
    public void remove(AnimalExampleImpl animalExampleImpl)
    {
        prey.remove(animalExampleImpl);
    }
  
    public AnimalExampleImpl(Integer id, String name, String call)
    {
        this.id = id;
        this.name = name;
        this.call = call;
        prey = new ArrayList<AnimalExampleImpl>();
    }
    public static void main(String[] args)
    {}
    private List<AnimalExampleImpl> prey = new ArrayList<AnimalExampleImpl>();
    private Integer id;
    private String name, call;
    
}
